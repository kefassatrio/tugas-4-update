import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { MongoClient } from "mongodb";

const EXPRESS_PORT = 7879

const uri =
  "mongodb+srv://kefassatrio:9ze559AMMBsToTTd@cluster0.hwob7.mongodb.net/?retryWrites=true&w=majority";
const client = new MongoClient(uri);

const expressApp = express()
expressApp.use(cors())
expressApp.use(bodyParser.urlencoded({ extended: false }))
expressApp.use(bodyParser.json())

async function run() {
    try {
        await client.connect();
        const database = client.db('tugas4');
        const mahasiswaCollection = database.collection('mahasiswa');

        expressApp.post('/', (req, res) => {
            var mahasiswa = { nama: req.body.nama, npm: req.body.npm }; 

            (async () => {
                await mahasiswaCollection.insertOne(mahasiswa, (err, created) => {
                    if(err) {
                        console.log(err)
                        return res.json({status: 'NOT OK'});
                    }
                    return res.json({status: 'OK'});
                })
            })();
        })

        expressApp.listen(EXPRESS_PORT, () => console.log(`(express app) listening on port ${EXPRESS_PORT}!`))
    } finally {
        // await client.close();
    }
}

run().catch(console.dir);